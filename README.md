# Deep File Info

Want to find as much information about a given file before opening it on a POSIX-compatible system? Look no more.

Depending on a detected file type (file extensions are used only in some scenarios) we try to provide relevant information.

Note, that the amount of information may depend on other installed software. The script may recommend software for installation to get more information.
